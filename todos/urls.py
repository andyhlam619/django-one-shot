from django.urls import path
from todos.views import (
    todo_list,
    todo_list_detail,
    create_todo_list,
    edit_todo_list,
    delete,
    todo_item_create,
    edit_todo_item,
)


urlpatterns = [
    path("", todo_list, name="todo_list"),
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("create/", create_todo_list, name="create_todo_list"),
    path("<int:id>/edit/", edit_todo_list, name="edit_todo_list"),
    path("<int:id>/delete/", delete, name="delete"),
    path("add/", todo_item_create, name="todo_item_create"),
    path("<int:id>/item/edit/", edit_todo_item, name="edit_todo_item"),
]
