from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.

#################################################################################
# To view all todo lists
def todo_list(request):
    todos = TodoList.objects.all()
    context = {"todo_list": todos}
    return render(request, "todos/list.html", context)


#################################################################################
# To view one specific todo list
def todo_list_detail(request, id):
    todos = get_object_or_404(TodoList, id=id)
    context = {
        "todo_detail": todos,
    }
    return render(request, "todos/details.html", context)


#################################################################################
# To create a new todo LIST
def create_todo_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list")
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


#################################################################################
# To create a new todo ITEM


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/add.html", context)


#################################################################################
# To edit todo LIST
def edit_todo_list(request, id):
    todos = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todos)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=todos)

    context = {
        "form": form,
        "todo_detail": todos,
    }

    return render(request, "todos/edit.html", context)


#################################################################################
# To edit todo ITEM
def edit_todo_item(request, id):
    todos = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todos)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm(instance=todos)

    context = {
        "form": form,
        "todo_detail": todos,
    }

    return render(request, "todos/tedit.html", context)


#################################################################################
# To delete a todo list
def delete(request, id):
    todos = TodoList.objects.get(id=id)
    if request.method == "POST":
        todos.delete()
        return redirect("todo_list")

    return render(request, "todos/delete.html")
